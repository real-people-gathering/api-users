import Joi from '@hapi/joi';

const schemas = {
  createUser: Joi.object({
    id: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
    fullname: Joi.string()
      .max(50)
      .required(),
    username: Joi.string()
      .max(50)
      .required(),
    mail: Joi.string()
      .email()
      .required(),
    phone: Joi.string().pattern(/^\d{10}$/), // Match a common phone format (ex: 0685567412)
  }),
  follow: Joi.object({
    idCurrentUser: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
    idTargetUser: Joi.string()
      .guid({ version: 'uuidv4' })
      .required()
      .invalid(Joi.ref('idCurrentUser')), // ID must be different
  }),
  unfollow: Joi.object({
    idCurrentUser: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
    idTargetUser: Joi.string()
      .guid({ version: 'uuidv4' })
      .required()
      .invalid(Joi.ref('idCurrentUser')), // ID must be different
  }),
  userID: Joi.object({
    id: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
  }),
};

export default schemas;
