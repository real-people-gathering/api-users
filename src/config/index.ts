import dotenv from 'dotenv';

const envFound = dotenv.config();
if (!envFound) {
  throw new Error('Could not find .env file !');
}

export default {
  port: process.env.PORT || 5002,
  env: process.env.NODE_ENV || 'production',
  db: {
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    name: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    schema: process.env.DB_SCHEMA,
  },
  logs: {
    level: process.env.LOG_LEVEL || 'info',
  },
};
