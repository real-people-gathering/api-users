import { Keycloak } from 'keycloak-connect';
import { Router } from 'express';

import db from '../dal/users';
import schemas from '../config/schemas';
import valid from '../loaders/validation';

export default (app: Router, keycloak: Keycloak) => {
  const route = Router();

  route.get('/status', (req, res) => {
    res.status(200).send();
  });

  route.get('/', keycloak.protect(), (req, res, next) => {
    db.getUsers()
      .then(result => res.status(200).json(result.rows))
      .catch(next);
  });

  route.get('/:id', keycloak.protect(), valid(schemas.userID, 'params'), (req, res, next) => {
    const idUser = req.params.id;
    db.getUserById(idUser)
      .then(result => res.status(200).json(result.rows[0]))
      .catch(next);
  });

  route.post('/', keycloak.protect(), valid(schemas.createUser, 'body'), (req, res, next) => {
    const { id, fullname, username, mail, phone } = req.body;
    db.createUser(id, username, fullname, mail, phone)
      .then(result => res.status(201).send(result.rows[0].id))
      .catch(next);
  });

  route.put(
    '/:id',
    keycloak.protect(),
    valid(schemas.userID, 'params'),
    valid(schemas.createUser, 'body'),
    (req, res, next) => {
      const idUser = req.params.id;
      const { fullname, username, mail, phone } = req.body;
      db.updateUser(idUser, fullname, username, mail, phone)
        .then(result => res.status(200).send())
        .catch(next);
    },
  );

  route.delete('/:id', keycloak.protect(), valid(schemas.userID, 'params'), (req, res, next) => {
    const idUser = req.params.id;
    db.deleteUser(idUser)
      .then(result => res.status(200).send())
      .catch(next);
  });

  route.post('/follow', keycloak.protect(), valid(schemas.follow, 'body'), (req, res, next) => {
    const { idCurrentUser, idTargetUser } = req.body;
    db.follow(idCurrentUser, idTargetUser)
      .then(result => res.status(200).send())
      .catch(next);
  });

  route.post('/unfollow', keycloak.protect(), valid(schemas.unfollow, 'body'), (req, res, next) => {
    const { idCurrentUser, idTargetUser } = req.body;
    db.unfollow(idCurrentUser, idTargetUser)
      .then(result => res.status(200).send())
      .catch(next);
  });

  route.get(
    '/:id/followers',
    keycloak.protect(),
    valid(schemas.userID, 'params'),
    (req, res, next) => {
      const idUser = req.params.id;
      db.getAllFollowingUsers(idUser)
        .then(result => res.status(200).json(result.rows))
        .catch(next);
    },
  );

  route.get(
    '/:id/follows',
    keycloak.protect(),
    valid(schemas.userID, 'params'),
    (req, res, next) => {
      const idUser = req.params.id;
      db.getAllFollowedUsers(idUser)
        .then(result => res.status(200).json(result.rows))
        .catch(next);
    },
  );

  app.use('/users', route);
};
