import { Router } from 'express';
import users from './users';

export default keycloak => {
  const app = Router();
  users(app, keycloak);
  return app;
};
